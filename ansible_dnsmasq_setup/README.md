## Beschreibung

Script zum generieren von Dnsmasq-Konfigurations-Dateien. 

Die bereits generierten Dateien finden sich unter /pxeboot/dnsmasq/dhcp-host. Falls große Änderungen stattfinden wird die Nutzung des Scripts empfohlen, bei kleinen Änderungen können die Dateien unter /pxeboot/dnsmasq/dhcp-host auch direkt angepasst werden. 

Derzeit sind diese Dateien in die generellen Orte unterteilt, also:
- lzr.hosts
- tre.hosts

## Konfiguration

In ```configs``` finden sich die unterschiedlichen Cluster wieder. Die Cluster haben nur jeweils einen Unterpunkt: hosts, unter welchem eine Liste von Hosts mit *MAC, IP, hostname und tag* konfiguriert wird. Sämtliche Dateien im ```configs``` Ordner werden am Anfang als Variablen in das Playbook eingebunden und stehen somit zur verfügung. Unter dem Ordner ```templates``` finden sich die Templates der jeweiligen Umgebungen. Das Template ```dhcp-lzr``` greift auf die Variablen aller LZR-Cluster zu und bildet aus den Hosts dann eine dnsmasq-Konfigurations-Datei, in welcher vorerst alle Hosts einkommentiert, also deaktiviert sind.

Im Playbook müssen nur die zwei Variablen ```src```und ```dest``` ggf. angepasst werden. **dest** ist der Dateiname/Pfad der Outputdatei und **src** ist die Template-Datei: entweder dhcp-lzr oder dhcp-tre.

Falls z.B. ein neuer Host hinzugefügt werden soll, muss er lediglich in der korrekten Cluster-Datei unter ```configs``` hinzugefügt werden.

## Starten des Playbooks

```
ansible-playbook playbook.yml
```


