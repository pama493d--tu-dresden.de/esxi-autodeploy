## Dnsmasq:

Dnsmasq muss als funktionierender DHCP-Server installiert und konfiguriert werden.

```
sudo apt-get install dnsmasq
```

Im Unterorder dnsmasq befindet sich die derzeitige Dnsmasq-Konfiguration sowie die statisch generierten DHCP-Adressen der Hosts.

- dnsmasq.conf sollte unter /etc/dnsmasq.conf liegen
- Ordner dhcp-host sollte unter /etc/dnsmasq.d liegen

#### Dnsmasq für PXE & tftp konfiguieren → /etc/dnsmasq.conf

```
enable-tftp
tftp-root=/srv/tftpboot
tftp-unique-root=mac
dhcp-boot=tag:enterprise-lzr-devel-amd,current/efi/boot/bootx64.efi,,172.30.154.3
```

Das TFTP-Root-Directory ist ```/srv/tftpboot```. Durch die Angabe von ```tftp-unique-root=mac``` wird das TFTP-Root-Directory zu ```/srv/tftpboot/<client-mac-address>``` geändert. Jeder Client hat also seinen eigenen TFTP-Boot-Ordner, aus dem er die Dateien zieht. In diesem Ordner sucht Dnsmasq dann nach der Datei ```current/efi/boot/bootx64.efi,```, dem Bootloader, welcher die ```boot.cfg``` im TFTP-Root-Directoy (```/srv/tftpboot/<client-mac-address>```) einliest.  (Siehe nächste Schritte)


#### Image via TFTP/PXE für einen Cluster zur Verfügung stellen 

- Ein beliebiges ESXI-ISO-Image muss heruntergeladen werden.
- Beispiel: ESXI7 soll für den Cluster 'beispiel-cluster' zur Verfügung gestellt werden:
  - Das ESXI-ISO-Image wird unter /mnt gemountet.
  - Die Ordner für das Beispielcluster und das Image werden angelegt.
  - Der Inhalt des Images wird in den Image-Ordner unter *beispiel-cluster/esxi7* kopiert.


```
mount -o loop ESXI.iso /mnt
mkdir -p /srv/tftpboot/beispiel-cluster/esxi7
cp -rf /mnt/* /srv/tftpboot/beispiel-cluster/esxi7/.
```

- Da ein einfaches verändern der ESX-Versionen möglich sein soll, wird die ESX-Version, welche ausgeliefert werden soll nach einem Ordner namens 'current' gelinkt. Der Client bezieht dann aus diesem Ordner die benötigten Boot-Dateien.
```
link -s /srv/tftpboot/beispiel-cluster/esxi7 /srv/tftpboot/beispiel-cluster/current
```
- Falls es z.B. ein Update von esxi7 nach esxi8 geben soll, muss das esxi8-ISO-Image im Unterordner esxi8 entpackt werden. Danach sollte der bisherige 'current'-Link gelöscht und ein neuer angelegt werden, welcher auf esxi8 zeigt.

#### Konfiguration der boot.cfg

- Die boot.cfg MUSS im Root-TFTP-Ordner liegen. Also z.B. direkt im Ordner des jeweiligen Clusters. Die boot.cfg findet sich im Image unter ```efi/boot/boot.cfg```.
- In der boot.cfg finden sich Trailing-Slashes, welche für eine korrekte Ordner-Findung gelöscht werden müssen.

```
cp /srv/tftpboot/beispiel-cluster/esxi7/efi/boot/boot.cfg /srv/tftpboot/beispiel-cluster/boot.cfg
sed -i 's#/##g' /srv/tftpboot/beispiel-cluster/boot.cfg
```

- **Richtiges Prefix in der boot.cfg setzen:**
  - Der prefix sollte der Ordner sein, in welchem das ausgepackte ESXI-Image liegt.
  - Aus vorher genannten Gründen sollte der prefix deshalb immer 'current' sein.
  - *boot.cfg*
    - ```prefix=current```
- **Konfiguration der Kickstart-Datei:**
  - Ermöglicht die automatische Installation von ESXI ohne Nutzereingabe.
  - Die Kickstart-Datei (ks.cfg) kann via http, https, ftp, nfs, cdrom, usb, ... zur Verfügung gestellt werden.
  - *boot.cfg*
    - ```kernelopt=ks=https://172.30.154.3/ks.cfg```
- **Inhalt der Kickstart-Datei:**
  - siehe ks.cfg
  - Encrypted (hashed) root passwort in ks.cfg
    - generieren eines hashed passworts:
      - ```openssl passwd -6 $ROOT_PASSWORD```
        - -1 = md5
        - -5 = sha256
        - -6 = sha512 (am sichersten) 
    - Anpassen des root-passworts:
      - ```rootpw --iscrypted $1$fgJ5Imnx$XPCH6KFUC.39EFG7gMB4g0```


#### Beispiel-Aufbau des TFTP-Root-Verzeichnises:

Es gibt 2 cluster mit den Namen *cluster1* und *cluster2*, in jedem Cluster befinden sich je 2 esxi-Versionen, von denen jeweils die neueste Version für den PXE-Boot verwendet wird. Die Clients werden dann zum dazugehörigen Cluster gelinkt (vorzugsweise automatisch via dnsmasq-checker).

- **cluster1**
  - *boot.cfg* (prefix=current)
  - *esxi7*
    - efi/boot/bootx64.efi
    - *Rest vom ESXI-ISO-Image*
  - *esxi6*
    - efi/boot/bootx64.efi
    - *Rest vom ESXI-ISO-Image*
  - *current → esxi7* (link)
- **cluster2**
  - *boot.cfg* (prefix=current)
  - *esxi7*
    - efi/boot/bootx64.efi
    - *Rest vom ESXI-ISO-Image*
  - *esxi8*
    - efi/boot/bootx64.efi
    - *Rest vom ESXI-ISO-Image*
  - *current → esxi8* (link)
- **fc-34-97-b1-68-c4 → cluster1** (link, siehe dnsmasq-checker)
- **fc-34-97-b1-0b-53 → cluster1** (link, siehe dnsmasq-checker)
- **fc-34-97-b1-0b-2b → cluster2** (link, siehe dnsmasq-checker)


