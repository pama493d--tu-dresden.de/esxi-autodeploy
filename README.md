esxi-autodeploy
===

## Setup & Erklärung der Komponenten

#### 1. Installation von Ansible und Aktivieren des Virtual-Environments

```
sudo apt-get install python3-venv
python3.9 -m venv venv_ansible
source venv_ansible/bin/activate
pip install ansible
```

Erstellt ein Python-Virtual-Environment im derzeitigen Ordner. Der Befehl source venv_ansible/bin/activate muss nach jedem logout wieder ausgeführt werden, um ins Virtual-Environment zu kommen und Zugriff auf die ansible-* Befehle zu haben. Alle Befehle in 'ansible_dnsmasq_setup' & 'ansible_host_setup' setzen voraus, dass Ansible erfolgreich installiert & das Virtual-Environment aktiv ist.

**Im vorhandenen Setup sind bereits Ansible inklusive aller Dependencies vorinstalliert und das Virtual-Environment wird automatisch beim Login des Nutzers *ansible* aktiviert.**

#### 2. Setup & Konfiguration von Dnsmasq, PXE-Boot & Kickstart 

- Konfiguration von Dnsmasq, Einstellungen für PXE-Boot, Vorbereiten des ESXI-Images zur Übertragung, Konfiguration des ESXI-Images, Automatische Installation via Kickstart
- *→ siehe ```pxeboot```*

#### 3. Konfiguration des automatischen Hinzufügens eines Hosts zur Dnsmasq-Config 

- Erstellt eine Dnsmasq-Config-Datei aus einer Liste vorgegebener Hosts & Cluster. Die bereits durch das Script generierten Dateien für die jeweiligen Standorte (lzr.hosts, tre.hosts) liegen unter ```pxeboot/dnsmasq/dhcp-host```. Bei kleinen Änderungen sollten diese Dateien am besten direkt angepasst werden.
- *→ siehe ```ansible_dnsmasq_setup```*

#### 4. Konfiguration des Automatischen Erstellens von Symlinks von MAC-Adresse zu Cluster nach Änderung der Dnsmasq-Client-Configs

- Überwacht die Dnsmasq-Konfigurationen der statischen DHCP-Clients und erstellt bei einer erkannten Änderung neue Symlinks. Die Aufgabe der Symlinks ist es, die MAC-Adresse des Clients zum richtigen Cluster zu linken, in welchem die benötigten Dateien für den PXE-Boot liegen. TFTP greift auf die benötigten Dateien unter dem Symlink zu (tftproot=tftproot+client_mac).
- *→ siehe ```dnsmasq_checker```*

#### 5. Konfiguration des automatischen Aufsetzens eines Hosts im VCenter 

- Komplettes Setup eines Hosts innerhalb des VCenters via Ansible.
- **Soll später im ansible-Repository (https://gitlab.hrz.tu-chemnitz.de/zih/all/ansible) liegen, derzeit keine bzw. kaum Doku. Derzeit im Branch feature/esx-server im ansible-Repository.**
- *→ siehe ```ansible_host_setup```*


## Vorhandenes Setup & Komponenten (172.30.154.3)

### Allgemein

**Nutzer:** *ansible*

**Repository-Ordner:** ```/usr/local/ansible/esxi-autodeploy```

**Updaten des Repositories auf neusten Stand:**

- Als Nutzer ansible:
  ```
  cd /usr/local/ansible/esxi-autodeploy
  git pull
  ```
- Falls Änderungen im Repository-Ornder vorgenommen wurden vorher: ```git stash``` (verwirft die lokalen Änderungen)


### Ansible

→ Ansible-Skripte unter ansible_dnsmasq_setup oder ansible_host_setup

**Virtual-Environment-Ordner:** ```/home/ansible/ansible_venv```

**Das Virtual-Environment wird automatisch beim Login des Nutzers *ansible* aktiviert.**

- Vor Ausführen der Playbooks sollte der SSH-Key mit ssh-add gespeichert werden, sonst kommt beim Verbinden via SSH ein Passphrase-Prompt (Nutzer: ansible):
  ```
  eval "$(ssh-agent -s)"
  ssh-add ~/.ssh/id_rsa
  ```

### Dnsmasq

→ gibt dem Host eine DHCP-Adresse und liefert die nötigen Boot-Dateien via TFTP

**Konfigurations-Datei:** ```/etc/dnsmasq.conf```

**DHCP-Client-Konfigurationen:** ```/etc/dnsmasq.d/dhcp-host/hosts.hosts```

**TFTP-Root-Verzeichnis:** ```/srv/tftpboot```

### Dnsmasq-Checker

→ überwacht DHCP-Konfigurationen und erneuert Symlinks

**Service-Datei:** ```/etc/systemd/system/dnsmasq-checker.service```

**Python-Script:** ```/usr/local/ansible/esxi-autodeploy/dnsmasq_checker/dnsmasq-config-changed.py```

**Config-Datei**: ```/usr/local/ansible/esxi-autodeploy/dnsmasq_checker/config.json```

### Nginx

→ stellt die Kickstart-Datei zur verfügung (ks.cfg)

**Website-Konfiguration:** ```/etc/nginx/sites-available/serve-files```

**Root-Datei-Verzeichnis:** ```/srv/http```

### vCenter - Permissions

**Nutzer:** *autodeploy*

Der Nutzer autodeploy wird der Rolle *Ansible Deployment Server* zugewiesen.

**Permissions der Rolle 'Ansible Deployment Server':**

- Permissions
  - Modify permission
- Distributed switch
  - Host operation
- Global
  - Licenses
- Host
  - Configuration
    - Advanced settings
    - Change date and time settings
    -  Change settings
    - Connection
    - Image configuration
    - Maintenance
    - Network configuration
    - Power
    - Query patch
    - Security profile and firewall
    - Storage partition configuration
    - System Management
  - Inventory
    - Add host to cluster
- Network
  - Assign network
- VMware vSphere Lifecycle Manager
  - Desired Configuration Management Privileges
    - Read-only access to desired configuration management platform
    - Remediate cluster to the desired configuration.
  - ESXi Health Perspectives
    - Read
  - Lifecycle Manager: General Privileges
    - Read
  - Lifecycle Manager: Hardware Compatibility Privileges
    - Access Hardware Compatibility
  - Lifecycle Manager: Image Privileges
    - Read
  - Lifecycle Manager: Image Remediation Privileges
    - Read
    - Write
  - Lifecycle Manager: Settings Privileges
    - Read
  - Manage Baseline
    - Attach Baseline
    - Manage Baseline
  - Manage Patches and Upgrades
    - Remediate to Apply Patches, Extensions, and Upgrades
    - Scan for Applicable Patches, Extensions, and Upgrades
    - Stage Patches and Extensions
    - View Compliance Status









