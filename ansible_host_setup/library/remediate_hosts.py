#!/usr/bin/python
from ansible.module_utils.basic import AnsibleModule
import warnings
import base64
import requests
import urllib3
import json

# converts given clustername and hostname to their identifiers
# triggers a compliance-check on the cluster and updates given host if needed (the identifiers are needed for this)

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
warnings.filterwarnings("ignore", category=DeprecationWarning)


SUCCESS_STATUS_CODE = [200, 201, 202]


def get_call(hostname, endpoint, headers) -> tuple[bool, str]:
    """→(error, response_text)"""
    response = requests.get(
        "https://" + hostname + endpoint,
        headers=headers,
        verify=False,
    )
    if response.status_code in SUCCESS_STATUS_CODE:
        return (False, json.loads(response.text))
    else:
        return (True, str(response.text))


def post_call(hostname, endpoint, headers, body=None) -> tuple[bool, str]:
    """→(error, response_text)"""
    response = requests.post(
        "https://" + hostname + endpoint,
        json=body,
        headers=headers,
        verify=False,
    )
    if response.status_code in SUCCESS_STATUS_CODE:
        return (False, json.loads(response.text))
    else:
        return (True, str(response.text))


def run_module():
    module_args = dict(
        hostname=dict(type="str", required=True),
        username=dict(type="str", required=True),
        password=dict(type="str", required=True, no_log=True),
        cluster=dict(type="str", required=True),
        host=dict(type="str", required=True),
    )

    result = dict(changed=False, msg="")
    module = AnsibleModule(argument_spec=module_args, supports_check_mode=False)
    hostname = module.params["hostname"]
    username = module.params["username"]
    password = module.params["password"]
    cluster = module.params["cluster"]
    host = module.params["host"]

    cluster_identifier = ""
    host_identifier = ""

    # authenticate
    b64 = base64.b64encode(f"{username}:{password}".encode("ascii")).decode()
    init_header = {"Authorization": "Basic " + b64}
    error, response_text = post_call(hostname, "/api/session", init_header)
    if error:
        result["msg"] = f"API Error: {response_text}"
        module.fail_json(**result)
    token = response_text
    headers = {"vmware-api-session-id": token, "Content-type": "application/json"}

    # get identifier of given cluster (convert clustername to cluster-identifier)
    error, response_text = get_call(hostname, "/api/vcenter/cluster", headers)
    if error:
        result["msg"] = f"API Error: {response_text}"
        module.fail_json(**result)
    for temp_cluster in response_text:
        if temp_cluster["name"] == cluster:
            cluster_identifier = temp_cluster["cluster"]
    if cluster_identifier == "":
        # fail if cluster identifier not found
        result["msg"] = f"Error: Cluster not found."
        module.fail_json(**result)

    # get identifiers of host (convert hostname to host-identifier)
    error, response_text = get_call(hostname, "/api/vcenter/host", headers)
    if error:
        result["msg"] = f"API Error: {response_text}"
        module.fail_json(**result)
    for temp_host in response_text:
        if temp_host["name"] == host:
            host_identifier = temp_host["host"]
    if host_identifier == "":
        # fail if one or more hosts haven't been found
        result["msg"] = f"Error: Host not found."
        module.fail_json(**result)

    # post call for remediation
    body = {"accept_eula": True, "hosts": [host_identifier]}
    error, response_text = post_call(
        hostname,
        f"/api/esx/settings/clusters/{cluster_identifier}/software?action=apply&vmw-task=true",
        headers,
        body,
    )
    if error:
        result["msg"] = f"API Error: {response_text}"
        module.fail_json(**result)

    result["changed"] = True
    result["msg"] = f"{hostname}"
    module.exit_json(**result)


def main():
    run_module()


if __name__ == "__main__":
    main()
