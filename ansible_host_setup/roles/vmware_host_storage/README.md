vmware_host_storage
=========

Setup host-network-configuration:

- Add Host to Cluster (run on each role, used to confirm if host is actually available)
- Enter maintenance mode (run on each role, host must be in maintenance)
- Enable ISCSI-Adapter
- Setup a list of ISCSI-Dynamic-Discovery-IP-Addresses and assign IQN from IQN-Map
- Rescan Storage
- Add a list of NFS-Datastores to Host
- Remove Datastore remote-install-location

Requirements
------------

```
ansible-galaxy collection install community.vmware
pip install pyvmomi
pip install --upgrade git+https://github.com/vmware/vsphere-automation-sdk-python.git
```

