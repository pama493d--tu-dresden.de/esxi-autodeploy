vmware_host_general 
=========

Setup general host-configuration:

- Add Host to Cluster (run on each role, used to confirm if host is actually available)
- Enter maintenance mode (run on each role, host must be in maintenance)
- Set NTP-Servers for host
- Start & enable NTP-Service on host
- Start & enable SSH-Service on host
- Start & enable Syslog-Service on host
- Changed Advanced Configuration of host
- Set permission of 'DOM\Domänen-Benutzer' to read-only
- Set License-Key on host
- Copy file passthru.map to /etc/vmware
- Run specified SSH-Commands on host
- Change host root-pw
- Trigger Cluster-Compliance-Check & Remediate Host

Requirements
------------

```
ansible-galaxy collection install community.vmware
pip install pyvmomi
pip install --upgrade git+https://github.com/vmware/vsphere-automation-sdk-python.git
```

