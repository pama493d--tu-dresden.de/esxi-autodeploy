vmware_host_network
=========

Setup host-network-configuration:

- Add Host to Cluster (run on each role, used to confirm if host is actually available)
- Enter maintenance mode (run on each role, host must be in maintenance)
- Register host on Dvswitch and assign physical uplinks
- Reset Managment Adapter (required, otherwise host looses gateway)
- Setup VMkernel Adapters
- Disable IPv6
- Set DNS-Servers

Requirements
------------

```
ansible-galaxy collection install community.vmware
pip install pyvmomi
pip install --upgrade git+https://github.com/vmware/vsphere-automation-sdk-python.git
```

