## Beispiel

Einzelner Server

```
ansible-playbook playbooks/vmware_host_setup.yml --extra-vars "@auth.yml" -l \*k411\* --ask-vault-password
```

Gesamter Cluster

```
ansible-playbook playbooks/vmware_host_setup.yml --extra-vars "@auth.yml" -l \*enterprise_lzr_devel_amd\* --ask-vault-password
```

## Dependencies

```
ansible-galaxy collection install community.vmware
pip install pyvmomi
pip install --upgrade git+https://github.com/vmware/vsphere-automation-sdk-python.git
```

## Beschreibung

#### library Ordner

Beinihaltet selbst geschriebene Ansible-Module. 

**remediate_hosts**

Triggered einen Compliance-Check auf dem Cluster und Updated die angegebenen Hosts (falls nötig). Wartet nicht, bis der Task abgeschlossen ist → kann Errors werfen, welche nicht in Ansible angezeigt werden.

Args:
- hostname: vcenter hostname
- username: vcenter username
- password: vcenter password
- cluster: cluster, auf welchem der check passiert
- hosts: liste an hosts, welche geupdatet werden sollen (falls nötig)

#### playbook.yml

Im Playbook stehen die Dinge drin, die gemacht werden sollen. Das Playbook sollte nicht direkt angepasst werden.

**Folgende Aufgaben werden ausgeführt**:
- Host: zum Cluster hinzufügen
- Host: an DvSwitch registrieren, Uplink zum physischen Adapter hinzufügen
- Host: Management-Adapter vmk0 vor Umstellung auf static vorbereiten
- Host: Weitere Management-Adapter hinzufügen bzw. vmk0 bearbeiten
- Host: IPv6 im TCP/IP-Stack ausschalten
- Host: DNS-Server setzen
- Host: ISCSI anschalten
- Host: ISCSI-Dynamic-Discovery-IP-Adressen hinzufügen und ISCSI-IQN setzen
- Host: Storage, VMFS & HBA Rescan
- Host: Extra Datastores an Host anfügen (z.B. NFS)
- Host: NTP-Einstellungen setzen
- Host: Service NTP anschalten und starten
- Host: Service SSH anschalten und starten
- Host: Service Syslog anschalten und starten
- Host: "Domänen-Benutzer"-Berechtigungen auf Read-Only setzen
- Host: Lizens hinzufügen
- Host: Firewall-Ports für Syslog öffnen
- Host: Advanced-Configurations setzen
- Host: Datastore "remote-install-location" löschen (wird automatisch nach pxe boot hinzugefügt)
- Host: SSH-Kommandos ausführen
- Host: Root-Passwort ändern (auch über SSH)
- Host & Cluster: Compliance Check am Cluster ausführen und angegebene Hosts Updaten
