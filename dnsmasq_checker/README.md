## Beschreibung

Stellt einen Service bereit, der permanent die Änderungen innerhalb einer Dnsmasq Config-Datei oder -Ordner überwacht. Falls eine Änderung gefunden wurde, wird das Script dnsmasq-config-changed.py ausgeführt. Dieses Script löscht alle bisherigen Symlinks und generiert neue basierend auf den überwachten Dateien.

Derzeit ist der Service sowie das Script darauf ausgelegt, dass nur diese eine Datei überwacht wird: ```/etc/dnsmasq.d/dhcp-host/hosts.hosts``` .


Das Script erwartet, dass der innere Aufbau der Dnsmasq-Konfiguration wie folgt aussieht:

```
fc:34:97:b1:0b:2b,server-k411.lzr.zih.tu-dresden.de,172.30.154.188,5m,set:enterprise-lzr-devel-amd
fc:34:97:b1:0b:53,server-k412.lzr.zih.tu-dresden.de,172.30.154.189,5m,set:enterprise-lzr-devel-amd
# fc:34:97:b1:68:c4,server-k413.lzr.zih.tu-dresden.de,172.30.154.190,5m,set:enterprise-lzr-devel-amd
```

Das Script generiert aus **tftp-root** + **MAC-Adresse** den Pfad des Links (Destination) und aus **tftp-root** + **tag (*enterprise-lzr-devel-amd*)** die Source des Links.

Alle kommentierten Einstellungen (mit **#**) werden ignoriert und für diese werden keine Links angelegt.

## Konfiguration dnsmasq-checker.service:

- Pfad zum Python-Script anpassen
- Pfad zur Config-Datei anpassen

**Setup**

Der Service benötigt das Programm 'entr':

```
sudo apt-get install entr
```

Der Service muss als Systemd Service gestartet werden:

```
cp dnsmasq-checker.service /etc/systemd/system
systemctl daemon-reload
systemctl start dnsmasq-checker.service
```

**Falls der Service eine einzelne Datei überprüft (wie derzeit eingestellt), dann darf diese Datei NICHT gelöscht werden, da sonst der Service crashed und neu gestartet werden muss.**

## Konfiguration dnsmasq-config-changed.py:

Die Konfiguration findet über eine json-Config-Datei statt, welche mit dem Argument *--config* an das Programm gegeben wird.

**config.json**

- **log**: *(true|false)* Ob logs in eine Logdatei geschrieben werden sollen. Falls die Einstellung auf *true* gesetzt ist, wird auch eine korrekte Konfiguration der **logfile** erwartet. Das Script schreibt dann logs in die **logfile** als auch auf stdout. Falls die Einstellung auf *false* gesetzt ist, werden logs nur auf stdout geschrieben.

- **logfile**: *(str)* Pfad zu einer Logdatei. Der Nutzer des ausführenden Programms braucht Schreibrechte zu diesem Pfad.

- **is_directory**: *(true|false)* Gibt an, ob der **dnsmasq_config_path** ein Ordner ist oder nicht. Falls diese Einstellung auf *true* gesetzt ist, muss der **dnsmasq_config_path** ein Pfad zu einem Ordner sein. Das Script liest dann alle Dateien in diesem Ordner ein und behandelt diese als Dnsmasq-Konfigurationen. Falls die Einstellung auf *false* gesetzt ist, muss der **dnsmasq_config_path** ein Pfad zu einer Datei sein und es wird nur diese einzige Datei vom Script eingelesen und behandelt. Je nachdem ob eine einzelne Datei oder ein Ordner gecheckt wird, muss auch der ***dnsmasq-checker.service*** angepasst werden. Derzeit ist das Script sowie der Service darauf ausgelegt die einzelne Datei ```/etc/dnsmasq.d/dhcp-host/hosts.hosts``` zu überprüfen.

- **dnsmasq_config_path**: *(str)* Pfad zum Dnsmasq Config-Ordner / -Datei

- **tftp_root**: *(str)* Pfad zum TFTP-Root-Ordner




