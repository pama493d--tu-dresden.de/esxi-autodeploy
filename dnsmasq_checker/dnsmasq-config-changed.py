from os import listdir, symlink, remove
from os.path import isfile, join, islink
from datetime import datetime
import json
import argparse

LOG = False
LOG_PATH = ""


def log(text):
    time = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    print(f"{time}: {text}")
    if LOG:
        with open(LOGFILE, "a") as log:
            log.write(f"{time}: {text}\n")


# parse args
parser = argparse.ArgumentParser(
    prog="dnsmasq-config-changed",
    description="Is Executed by dnsmasq-checker.service if a change was registered",
)
parser.add_argument("-c", "--config")
args = parser.parse_args()
if not isfile(args.config):
    log("Config not found. Exiting.")
    exit(1)

# parse config
with open(args.config, "r") as f:
    config = json.load(f)

LOG = config["log"]
LOGFILE = config["logfile"]

dnsmasq_config_path = config["dnsmasq_config_path"]
is_directory = config["is_directory"]
tftp_root = config["tftp_root"]

log("#### Change Registered. #####")

# clean up all existing links
existing_links = [f for f in listdir(tftp_root) if islink(join(tftp_root, f))]
for link in existing_links:
    symlink_path = join(tftp_root, link)
    remove(symlink_path)
    log(f"Removed Symlink {symlink_path}")

# create new links
try:
    if is_directory:
        # if is_directory, go over all files in the dnsmasq_config_path
        config_files = [
            f
            for f in listdir(dnsmasq_config_path)
            if isfile(join(dnsmasq_config_path, f))
        ]
        for config_file in config_files:
            with open(join(dnsmasq_config_path, config_file)) as file:
                lines = file.readlines()
                for line in lines:
                    # ignore comments
                    try:
                        firstchar = line.strip()[0]
                    except IndexError:
                        continue
                    if firstchar != "#":
                        # link_dest is the mac-adress from the dnsmasq-config
                        mac = line.split(",")[0].replace(":", "-").strip()
                        link_dest = join(tftp_root, mac)

                        # get link_src from the tag in dnsmasq-config (set:some-cluster)
                        tag_name = line.split("set:")[1].split(",")[0].strip()
                        link_src = join(tftp_root, tag_name)
                        symlink(link_src, link_dest)

                        log(f"Created symlink {link_src} → {link_dest}")
    else:
        # if not is_directory only open the file specified in dnsmasq_config_path
        config_file = dnsmasq_config_path
        with open(dnsmasq_config_path) as file:
            lines = file.readlines()
            for line in lines:
                # ignore comments
                try:
                    firstchar = line.strip()[0]
                except IndexError:
                    continue
                if firstchar != "#":
                    # link_dest is the mac-adress from the dnsmasq-config
                    mac = line.split(",")[0].replace(":", "-").strip()
                    link_dest = join(tftp_root, mac)

                    # get link_src from the tag in dnsmasq-config (set:some-cluster)
                    tag_name = line.split("set:")[1].split(",")[0].strip()
                    link_src = join(tftp_root, tag_name)
                    symlink(link_src, link_dest)

                    log(f"Created symlink {link_src} → {link_dest}")

except Exception as E:
    log("Something went super wrong. The script aborted without being finished.")
    log(f"The Error was: {E}")
    exit(1)
